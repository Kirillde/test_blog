<?php

namespace Services;

use Models\Csv;
use DataMappers\CsvMapper;

class Scan
{

    public function __construct() {}

	/**
	 * @param $dir
	 * @param array $result
	 * @return array
	 */
    public function searchCsv($dir, array &$result = []) : array {
        $dh = new \DirectoryIterator($dir);
        foreach ($dh as $item) {
            if (!$item->isDot()) {
                if ($item->isDir()) {
                    $this->searchCsv("$dir/$item", $result);
                } else {
                    if (preg_match("/^.*\.(csv)$/", $item->getFilename())) {
                        $result = array_merge($result, $this->preparingData($dir . "/" . $item->getFilename()));
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param string $path
     * @return array
     */
    protected function preparingData(string $path) : array {
        $handle = fopen($path, 'r');

        $data = [];
        while(($arr = fgetcsv($handle, 1000, ',')) !== false) {
            $data[] = $arr;
        }

        fclose($handle);

        $keys = array_shift($data);

        $result = [];
        foreach ($data as $row) {
            $csv = Csv::factory();
            $csv->setMapper(CsvMapper::getInstance());
            $result[] = $csv->fillClass(array_combine($keys, $row));
        }

        return $result;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function save(array $data) :bool {
        foreach($data as $csv) {
            if(!$csv->validation()) {
                echo $csv->validationErrorMessage() . PHP_EOL;
                continue;
            }

            if($csv->checkUnique()) {
                $csv->save();
            } else {
                $csv->update();
            }
        }

        return true;
    }
}