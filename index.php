<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('Config/Config.php');

$config = \Config\Config::getInstance();
$config->initSettings(__DIR__);

use Route\Router;
use Route\Request;

$router = new Router(new Request);
/*$router->getPaths();*/

$router->get('/cabinet', 'CabinetController@Index', function() {

});

$router->get('/cabinet/article/{id}', 'CabinetController@Article', function(){

});

