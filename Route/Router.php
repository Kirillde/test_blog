<?php

namespace Route;

class Router
{
    /**
     * @var Request
     */
	private $request;

    /**
     * @var array
     */
	private $supportedHttpMethods = [
		'GET',
		'POST',
        'PUT',
        'DELETE'
	];

    private $controller;

    /**
     * Router constructor.
     * @param Request $request
     */
	public function __construct(Request $request) {
		$this->request = $request;
	}

    /**
     * @param $name
     * @param $args
     * @return false
     */
	public function __call($name, $args) {
		list($route, $action, $method) = $args;

		if($this->request->requestUri != $route) {
			return false;
		}

		if(!in_array(strtoupper($name), $this->supportedHttpMethods)) {
			$this->invalidMethodHandler();
		}

		if(!empty($action)) {
		    $this->controller = $action;
        } else {
            $this->{strtolower($name)}[$this->formatRoute($route)] = $method;
        }
	}

    /**
     * @param $route
     * @return string
     */
	private function formatRoute($route) {
		$result = rtrim($route, '/');
		if ($result === '') {
			return '/';
		}
		return $result;
	}


	private function invalidMethodHandler() {
		header("{$this->request->serverProtocol} 405 Method Not Allowed");
	}

	private function defaultRequestHandler() {
		header("{$this->request->serverProtocol} 404 Not Found");
	}

	/**
	 * Resolves a route
	 */
	private function resolve() {
        if(!empty($this->controller)) {
            //TODO: Перенести вверх инициализацию контроллера
            preg_match('/(.*)@(.*)/', $this->controller, $matches);

            $pathController = "_Controllers_$matches[1]";
            $method = $matches[2];
            $pathController = str_replace('_', '\\', $pathController);

            $objectController = new $pathController();
            echo $objectController->$method($this->request);
        } else {    
        	if(empty($this->{strtolower($this->request->requestMethod)})) {
        		//TODO: редирект или ошибку вывести
        		return false;
	        }
            $methodDictionary = $this->{strtolower($this->request->requestMethod)};
            $formatedRoute = $this->formatRoute($this->request->requestUri);
            $method = $methodDictionary[$formatedRoute];

            if(is_null($method)) {
                $this->defaultRequestHandler();
                return;
            }
            echo call_user_func_array($method, [$this->request]);
        }
	}

	public function __destruct() {
		$this->resolve();
	}
}