<?php

namespace Route;

class Request implements RequestInterface
{
    /**
     * Request constructor.
     */
	public function __construct() {
		$this->bootstrapSelf();
	}

    /**
     * Add new properties
     */
	private function bootstrapSelf() {
		foreach($_SERVER as $key => $value) {
			$this->{$this->toCamelCase($key)} = $value;
		}
	}

    /**
     * @param $string
     * @return string
     */
	private function toCamelCase($string) : string {
		$result = strtolower($string);

		preg_match_all('/_[a-z]/', $result, $matches);
		foreach($matches[0] as $match) {
			$c = str_replace('_', '', strtoupper($match));
			$result = str_replace($match, $c, $result);
		}
		return $result;
	}

    /**
     * @return array
     */
	public function getData() : array {
        $result = [];
		if($this->requestMethod === 'GET') {
            foreach($_GET as $key => $value) {
                $result[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
		} else if ($this->requestMethod == 'POST') {
			foreach($_POST as $key => $value) {
				$result[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
			}
		}
		return $result;
	}
}