<?php

namespace Route;

interface RequestInterface
{
	public function getData() : array;
}