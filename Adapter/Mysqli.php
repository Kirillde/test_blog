<?php

namespace Adapter;

use Config\Config;

class Mysqli implements Adapter
{
    private $_mysqli;

    /**
     * @param Config $config
     * @return void
     */
    public function connect(Config $config) {
        $this->_mysqli = new \mysqli($config->host, $config->user, $config->password, $config->dbscheme);
    }

    /**
     * @param string $sql
     * @return array
     */
    public function fetch(string $sql) {
        $result = [];
        $query = $this->_mysqli->query($sql);
        while ($row = $query->fetch_assoc()) {
            $result[] = $row;
        }
        return $result;
    }

    /**
     * @param string $sql
     * @return mixed
     */
    public function save(string $sql) {
        return $this->_mysqli->query($sql);
    }

    /**
     * @param string $sql
     * @return bool
     */
    public function update(string $sql) : bool {
        return $this->_mysqli->query($sql);
    }
}