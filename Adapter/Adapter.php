<?php

namespace Adapter;

use Config\Config;

interface Adapter
{
    /**
     * @param Config $config
     * @return mixed
     */
    public function connect(Config $config);

    /**
     * @param string $str
     * @return mixed
     */
    public function fetch(string $str);


    /**
     * @param string $str
     * @return mixed
     */
    public function save(string $str);

    /**
     * @param string $str
     * @return mixed
     */
    public function update(string $str);
}