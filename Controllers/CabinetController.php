<?php

namespace Controllers;

use Route\Request;
use View\View;
use Models\Article;
use DataMappers\ArticleMapper;

class CabinetController
{
    public function index(Request $request) {

        $article = new Article();
        $article->setMapper(ArticleMapper::getInstance());

        return View::render('admin.list', ['articles' => $article->all()]);

    }

    public function article(Request $request) {
        return View::render('admin.article', ['article' => '']);
    }
}