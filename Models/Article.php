<?php

namespace Models;

class Article extends Model
{
    /**
     * @var integer $id
     */
    public $id;

    /**
     * @var string $title
     */
    public $title;

    /**
     * @var string $content
     */
    public $content;

    /**
     * Scored goals
     * @var \DateTime $date
     */
    public $date;

    /**
     * @return array
     */
    public function validationRules() :array {
        return [];
    }
}