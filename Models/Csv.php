<?php

namespace Models;

class Csv extends Model
{
    /**
     * @var integer $id
     */
    public $date;

    /**
     * @var string $name
     */
    public $geo;

    /**
     * @var integer $games
     */
    public $zone;

    /**
     * Scored goals
     * @var integer $scored
     */
    public $impressions;

    /**
     * Skipped goals
     * @var integer $skipped
     */
    public $revenue;


    /**
     * @return array
     */
    public function validationRules() :array {
        return [
            'date' => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
            'geo' => '/^[A-Z]+$/',
            'zone' => '/^\d*$/',
            'impressions' => '/^\d*$/',
            'revenue' => '/^\d*\.?\d*$/'
        ];
    }
}