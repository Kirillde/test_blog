<?php

namespace Models;

use Adapter\Adapter;
use DataMappers\Mapper;

class Model
{

    /**
     * @var string
     */
    private $errorMessage;

    /**
     * @var Adapter
     */
    private $mapper;

    /**
     * @param $data
     * @return $this
     */
    public function fillClass($data) {
        foreach ($data as $property => $value) {
            if(is_array($value)) {
                Model::fillClass($value);
            }
            if(property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    public function asArray() : array {
        $reflection = new \ReflectionClass($this);
        $properties = $reflection->getProperties(\ReflectionProperty::IS_PUBLIC);

        $result = [];
        foreach($properties as $property) {
            $result[$property->name] = $this->{$property->name};
        }

        return $result;
    }

    /**
     * @return Model
     */
    public static function factory() {
        return new static();
    }

    /**
     * @return bool
     */
    public function validation() : bool {
        $status = true;

        foreach($this->asArray() as $property => $value) {
            if(!preg_match($this->validationRules()[$property], $value)) {
                $this->errorMessage .= 'Property ' . $property . ' with value ' . $value . ' is not valid. ';
                $status = false;
            }
        }

        return $status;
    }

    /**
     * @return string
     */
    public function validationErrorMessage() : string {
        return $this->errorMessage;
    }

    /**
     * @return bool
     */
    public function save() : bool {
        return $this->mapper->save($this);
    }

    /**
     * @param Mapper $mapper
     */
    public function setMapper(Mapper $mapper) {
        $this->mapper = $mapper;
    }

    /**
     * @return bool
     */
    public function checkUnique() : bool {
        return $this->mapper->checkUnique($this);
    }

    /**
     * @return bool
     */
    public function update() : bool {
        return $this->mapper->update($this);
    }

    public function all() {
        return $this->mapper->all();
    }
}