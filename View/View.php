<?php

namespace View;

class View {

    /**
     * @param string $template
     * @param array $params
     * @return string
     */
    private static function fetchPartial(string $template, array $params = array()) : string {
        extract($params);
        ob_start();
            include __DIR__.'/'.$template.'.php';
        return ob_get_clean();
    }

    /**
     * @param string $template
     * @param array $params
     * @return string
     */
    private static function renderPartial(string $template, $params = array()) : string {
        echo static::fetchPartial($template, $params);
    }

    /**
     * @param string $template
     * @param array $params
     * @return string
     */
    private static function fetch(string $template, array $params = array()) : string {
        $content = static::fetchPartial($template, $params);
        return static::fetchPartial('layout', array('content' => $content));
    }

    /**
     * @param string $template
     * @param array $params
     */
    public static function render(string $template, $params = array()) {
        $path = str_replace('.', '/', $template);
        echo static::fetch($path, $params);
    }
}