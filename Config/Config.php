<?php

namespace Config;

class Config
{
	protected static $instance = null;

    public $host = 'localhost';
    public $user = 'root';
    public $password = '6842gh';
    public $dbscheme = 'test12';

	private function __construct() {

	}

	public static function getInstance() {
		if(self::$instance == null) {
			self::$instance = new Config();
		}

		return self::$instance;
	}

    public function initSettings(string $dir) {
        return spl_autoload_register(function ($class) use ($dir) {
            $prefix = '';
            $base_dir = $dir . '/';

            $len = strlen($prefix);

            if (strncmp($prefix, $class, $len) !== 0) {
                return;
            }

            $relative_class = substr($class, $len);
            $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

            if (file_exists($file)) {
                require $file;
            }
        });
    }
}