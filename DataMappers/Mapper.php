<?php

namespace DataMappers;

use Adapter\Adapter;
use Config\Config;
use Adapter\Mysqli;

abstract class Mapper
{
    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     * @var null|Mapper
     */
    private static $instance = null;

    /**
     * Mapper constructor.
     * @param Config $config
     * @param Mysqli $mysqli
     */
    public function __construct(Config $config, Mysqli $mysqli) {
	    $this->adapter = $mysqli;
	    $this->adapter->connect($config);
    }

    /**
     * @return Mapper|null|static
     */
    public static function getInstance() {
        if(self::$instance != null) {
            return self::$instance;
        }
        return new static();
    }
}