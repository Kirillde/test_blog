<?php

namespace DataMappers;

use Config\Config;
use Adapter\Mysqli;
use Models\Csv;

class CsvMapper extends Mapper
{
    public function __construct() {
       parent::__construct(Config::getInstance());
    }

    /**
     * @param Csv $csv
     * @return bool
     */
    public function save(Csv $csv) : bool {
        $keys = implode(', ', array_keys($csv->asArray()));
        $values = $csv->asArray();

        array_walk($values, function(&$value) {
            if(is_string($value)) {
                $value = "'" . $value . "'";
            }
        });
        $values = implode(', ', $values);

        return $this->adapter->save("INSERT INTO csv ($keys) VALUES ($values)");
    }

    /**
     * @return array
     */
    public function fetch() : array {
        return $this->adapter->fetch('SELECT * FROM csv');
    }

    /**
     * @param Csv $csv
     * @return bool
     */
    public function checkUnique(Csv $csv) : bool {
        return count($this->adapter->fetch("SELECT id FROM csv WHERE date='$csv->date' AND geo='$csv->geo' AND zone='$csv->zone'")) == 0;
    }

    public function update(Csv $csv) {
        return $this->adapter->update("
              UPDATE csv 
              SET impressions = impressions + $csv->impressions,  
              revenue = revenue + $csv->impressions
              WHERE date='$csv->date' 
              AND geo='$csv->geo' 
              AND zone='$csv->zone'
          ");
    }

}